<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QUAD</title>
    <link rel="icon" type="image/x-icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="node_modules/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="node_modules/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
    <script src="https://unpkg.com/typed.js@2.0.16/dist/typed.umd.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>

<body>
    <!-- header design -->
    <header id="myHeader">
        <div class="top-head">
            <a href="#home" class="logo"><img src="assets/images/logo.jpeg" alt=""></a>
            <div class="search-bar">
                <div class="search-box">
                    <input type="text" name="search" class="search" placeholder="Search">
                    <i class='bx bx-search-alt'></i>
                </div> 
            </div>
        </div>
        <div class="menu">
            <i class='bx bx-menu' id="btn-menu"></i> Menu
        </div>
        <div class="nav-links">
            <ul class="d-flex align-items-center mb-0">
                <li><a href="#" class="active-link">Home</a></li>
                <li><a href="#aboutus">About Quad</a></li>
                <li>
                    <a href="#">Meetings</a>
                    <ul class="sub-menu">
                        <li><a href="#">Leaders</a></li>
                        <li><a href="#">Foreign Ministers</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Areas of Cooperation</a>
                    <ul class="sub-menu">
                        <li><a href="#">Climate</a></li>
                        <li><a href="#">Critical and Emerging Technologies</a></li>
                        <li><a href="#">Cyber</a></li>
                        <li><a href="#">Health Security Partnership</a></li>
                        <li><a href="#">Infrastructure</a></li>
                        <li><a href="#">Space</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Initiatives</a>
                    <ul class="sub-menu">
                        <li><a href="#">Indo-Pacific Partnership for Maritime Domain Awareness (IPMDA)</a></li>
                        <li><a href="#">STEM Fellowships</a></li>
                        <li><a href="#">Counter-terrorism</a></li>
                    </ul>
                </li>
                <li><a href="#">Media & Resources</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </div> 
    </header> 